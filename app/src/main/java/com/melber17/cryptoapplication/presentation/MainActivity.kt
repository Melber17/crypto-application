package com.melber17.cryptoapplication.presentation

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.melber17.cryptoapplication.R

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}