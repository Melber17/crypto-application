package com.melber17.cryptoapplication.di

import android.app.Application
import com.example.cryptoapp.di.DataModule
import com.example.cryptoapp.di.ViewModelModule
import com.example.cryptoapp.di.WorkerModule
import com.melber17.cryptoapplication.presentation.CoinApp
import dagger.BindsInstance
import dagger.Component

@ApplicationScope
@Component(
    modules = [
        DataModule::class,
        ViewModelModule::class,
        WorkerModule::class
    ]
)
interface ApplicationComponent {

//    fun inject(activity: CoinPriceListActivity)
//
//    fun inject(fragment: CoinDetailFragment)
//
    fun inject(application: CoinApp)

    @Component.Factory
    interface Factory {

        fun create(
            @BindsInstance application: Application
        ): ApplicationComponent
    }
}