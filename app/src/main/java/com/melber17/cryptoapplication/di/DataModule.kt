package com.example.cryptoapp.di

import android.app.Application
import com.melber17.cryptoapplication.data.database.AppDatabase
import com.melber17.cryptoapplication.data.database.CoinInfoDao
import com.melber17.cryptoapplication.data.network.ApiFactory
import com.melber17.cryptoapplication.data.network.ApiService
import com.melber17.cryptoapplication.data.repository.CoinRepositoryImpl
import com.melber17.cryptoapplication.di.ApplicationScope
import com.melber17.cryptoapplication.domain.CoinRepository
import dagger.Binds
import dagger.Module
import dagger.Provides

@Module
interface DataModule {

    @Binds
    @ApplicationScope
    fun bindCoinRepository(impl: CoinRepositoryImpl): CoinRepository

    companion object {

        @Provides
        @ApplicationScope
        fun provideCoinInfoDao(
            application: Application
        ): CoinInfoDao {
            return AppDatabase.getInstance(application).coinPriceInfoDao()
        }

        @Provides
        @ApplicationScope
        fun provideApiService(): ApiService {
            return ApiFactory.apiService
        }
    }
}
