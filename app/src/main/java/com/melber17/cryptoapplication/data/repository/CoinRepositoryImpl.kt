package com.melber17.cryptoapplication.data.repository

import android.annotation.SuppressLint
import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.work.ExistingWorkPolicy
import androidx.work.WorkManager
import com.example.cryptoapp.data.mapper.CoinMapper
import com.melber17.cryptoapplication.data.database.CoinInfoDao
import com.melber17.cryptoapplication.data.workers.RefreshDataWorker
import com.melber17.cryptoapplication.domain.CoinInfo
import com.melber17.cryptoapplication.domain.CoinRepository
import javax.inject.Inject

class CoinRepositoryImpl @Inject constructor(
    private val mapper: CoinMapper,
    private val coinInfoDao: CoinInfoDao,
    private val application: Application
): CoinRepository {
    override fun getCoinInfoList(): LiveData<List<CoinInfo>> {
       return Transformations.map(coinInfoDao.getPriceList()) {
           it.map {
               mapper.mapDbModelToEntity(it)
           }
       }
    }

    override fun getCoinInfo(fromSymbol: String): LiveData<CoinInfo> {
        return Transformations.map(coinInfoDao.getPriceInfoAboutCoin(fromSymbol)) {
            mapper.mapDbModelToEntity(it)
        }
    }

    override fun loadData() {
      val workManager = WorkManager.getInstance(application)
        workManager.enqueueUniqueWork(
            RefreshDataWorker.NAME,
            ExistingWorkPolicy.REPLACE,
            RefreshDataWorker.makeRequest()
        )
    }
}